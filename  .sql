How many distinct genres are there in the dataset?
Description: This question helps understand the diversity of music genres represented in the dataset.

SELECT COUNT(*) AS num_genres
  FROM data_mart.genre_dim;

What is the average popularity of artists in the dataset?
Description: This question provides an overall view of the popularity level of artists.

SELECT AVG(artist_popularity) AS avg_artist_popularity
  FROM data_mart.artist_dim;

Which playlist has the most tracks in it?
Description: This question identifies the playlist with the highest number of tracks, indicating its popularity or extensive collection.

SELECT playlist_id, COUNT(*) AS num_tracks
  FROM data_mart.track_fact
  GROUP BY playlist_id
  ORDER BY num_tracks DESC
  LIMIT 1;

What is the most common key signature among tracks?
Description: This question reveals the predominant key signature used in the tracks, providing insights into musical composition
SELECT key, COUNT(*) AS num_tracks
  FROM data_mart.track_fact
  GROUP BY key
  ORDER BY num_tracks DESC
  LIMIT 1

Which album has the longest duration on average?
Description: This question identifies the album with the highest average track duration, indicating potentially longer or more elaborate compositions.
SELECT ald.album_name, AVG(tf.duration_ms) AS avg_duration_ms
  FROM data_mart.track_fact tf
  JOIN data_mart.album_dim ald ON tf.album_id = ald.album_id
  GROUP BY ald.album_name
  ORDER BY avg_duration_ms DESC
  LIMIT 1;